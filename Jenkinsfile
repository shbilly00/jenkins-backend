#!/usr/bin/env groovy

env.AWS_DEFAULT_REGION = 'ap-northeast-2'
env.ECR_REPO_NAME = 'bill_ecr_repo_bg'
env.ECS_CLUSTER = 'bill_ecs_fargate_cluster'
env.ECS_SERVICE = 'bill_ecs_fargate_service_03'

pipeline {
  agent any
  environment{
      UID = sh(returnStdout: true, script : 'aws sts get-caller-identity --query Account --output text' ).trim()
  }
  options {
    timestamps()
    timeout(time: 10, unit: 'MINUTES')
  }
  stages {
    stage('source checkout'){
      steps{
        echo '======================[Start Checkout]===================='
        // checkout SCM
      }
    }

    stage('unit test'){
      steps{
        catchError(buildResult: 'UNSTABLE', stageResult: 'FAILURE'){
            echo'=======================[Start Unit Test]===================='
            // sh 'mvn test'
        }
      }
      post{
        success {
          echo "All Unit test success"
        }
        failure {
          echo "Some unit test failed"
        }
      }
    }

    stage('build') {
      environment{
        GIT_SHORT = GIT_COMMIT.take(7)
        ECR_REPO_URI = "${env.UID}.dkr.ecr.${env.AWS_DEFAULT_REGION}.amazonaws.com/${env.ECR_REPO_NAME}"
      }
      steps {
        echo '====================[build start]=================='
        sh 'echo $ECR_REPO_URI'
        catchError(buildResult: 'FAILURE', stageResult: 'FAILURE'){
            sh 'aws ecr get-login-password --region ${AWS_DEFAULT_REGION} | docker login --username AWS --password-stdin "$UID.dkr.ecr.$AWS_DEFAULT_REGION.amazonaws.com" '
            sh '''mvn clean compile jib:build -Djib.to.tags="Jenkins-${BUILD_ID}-${GIT_SHORT}","latest" \
                                              -Djib.to.image="$UID.dkr.ecr.$AWS_DEFAULT_REGION.amazonaws.com/$ECR_REPO_NAME"
            '''
        }
      }
      post {
        success {
            echo "The build stage success."
        }
        failure {
            sh 'mvn clean'
            error('The build stage failed.')
        }
      }
    }

    stage('Deploy'){
      input {
        message 'proceed deploy?'
        parameters {
          choice choices: ['proceed', 'rollback', 'change'], name: 'isDeploy'
        }
      }

      steps {
        echo '====================[Deploy start]=================='
        sh 'aws ecs update-service --region ${AWS_DEFAULT_REGION} --cluster ${ECS_CLUSTER} --service ${ECS_SERVICE} --force-new-deployment'
      }
    }
  }
}
