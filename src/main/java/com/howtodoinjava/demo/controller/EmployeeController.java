package com.howtodoinjava.demo.controller;

import java.util.ArrayList;
import java.util.List;

import com.howtodoinjava.demo.model.Employee;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class EmployeeController {
	
	@GetMapping("/")
    public List<Employee> getEmployees() 
    {
		List<Employee> employeesList = new ArrayList<Employee>();
		employeesList.add(new Employee(1,"ver1e","gupta","howtodoinjava@aws.com"));
		employeesList.add(new Employee(2,"Kio","Yg","kio@csdsd.com"));
		employeesList.add(new Employee(3,"Bis32","nff","bill@sdsds4d.cm"));
		return employeesList;
    }

	@GetMapping("/employee")
    public List<Employee> getEmployees2() 
    {
		List<Employee> employeesList = new ArrayList<Employee>();
		employeesList.add(new Employee(1,"sff","pta","howtodoiva@gasgfg.som"));
		return employeesList;
    }

	@GetMapping("/error")
    public String getEmployees3Employees() 
    {
		return "employeesList";
    }

}
